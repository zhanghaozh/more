FROM openjdk:8-jdk-alpine
COPY target/*.jar app.jar
EXPOSE 10000
ENTRYPOINT ["java","-jar","/app.jar"]